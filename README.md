# Pomodoro
A simple cli interfaced pomodoro study timer. 

Edit config.h to change between 25 or 50 minute study sessions as well as 5 or 10 minute breaks.

Also, you can get help with 

` pomodoro -h `

# Compiling

` gcc pomodoro.c -o pomodoro -lncurses `
